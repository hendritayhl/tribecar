
from flask import Flask, jsonify, render_template
import os
from flask_sqlalchemy import SQLAlchemy
from database import database
from flask_migrate import Migrate

from flask_restful import  Api



class Config(object):
    DEBUG = True
    SECRET_KEY = os.urandom(12)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = "sqlite:///test_new.db"

app = Flask(__name__)
app.config.from_object(Config)
database.db = SQLAlchemy(app)


Migrate(app, database.db)
api = Api(app)
from resources.condoresource import *

from models.usersinfo import User
from resources.condounitresource import CondoUnitResource, CondoUnitResourceIndividual
from resources.visitorresource import VisitorResource, VisitorResourceIndividual, VisitorVisitResource, \
    VisitorUnitCountResource, VisitorUnitCheckoutResource, ReportManagementResource

from sqlalchemy import event

from sqlalchemy.engine import Engine
from sqlite3 import Connection as SQLite3Connection

from models.visitor import *

@event.listens_for(Engine, "connect")
def _set_sqlite_pragma(dbapi_connection, connection_record):
    if isinstance(dbapi_connection, SQLite3Connection):
        cursor = dbapi_connection.cursor()
        cursor.execute("PRAGMA foreign_keys=ON;")
        cursor.close()


resource_fields = {
    'name' : fields.String
}
class MainResource(Resource):
    @marshal_with(resource_fields, envelope='user')
    def get(self):
        return User.query.all()


@app.route("/blockmanagement")
def blockmanagement():
    return render_template('blockmanagement.html')

@app.route("/unitmanagement")
def unitmanagement():
    return render_template('condounitmanagement.html')

@app.route("/visitormanagement")
def visitormanagement():
    return render_template('visitormanagement.html')


@app.route("/reportmanagement")
def reportmanagement():
    return render_template('reportmanagement.html')

@app.route("/")
def home():
    return render_template('home.html')

api.add_resource(MainResource, "/")
api.add_resource(CondoResource, "/condoblocks")
api.add_resource(CondoResourceIndividual, "/condoblocks/<string:block_id>")

api.add_resource(CondoUnitResource, "/condounits")
api.add_resource(CondoUnitResourceIndividual, "/condounits/<string:block_id>/<string:unit_name>")


api.add_resource(VisitorResource, "/visitors")
api.add_resource(VisitorResourceIndividual, "/visitor/<string:contact_number>/<string:ic_no>")

api.add_resource(VisitorVisitResource, "/visitorvisits")
api.add_resource(VisitorUnitCountResource, "/visitorcountbyblockandunit")
api.add_resource(VisitorUnitCheckoutResource, "/visitorupdatecheckout/<int:visitor_unit_id>")

api.add_resource(ReportManagementResource, "/reports")



if __name__ == '__main__':
    app.run(host='0.0.0.0')
