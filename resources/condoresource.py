from flask import jsonify, request
from flask_restful import Resource,  marshal_with, fields, reqparse

from models.condoblock import *

from database import database
class CondoResourceIndividual(Resource):
    ConsoResource_fields ={
        'block_id' : fields.String,
        'block_name' : fields.String
    }

    @marshal_with(ConsoResource_fields, envelope='condoblock')
    def get(self,block_id):
        found_block=  CondoBlock.query.get(block_id)
        if found_block == None:
            return "Block not found", 404

        return found_block

    def delete(self, block_id):
        found_block = CondoBlock.query.get(block_id)
        if found_block == None:
            return "Block not found", 404

        database.db.session.delete(found_block)
        database.db.session.commit();

        return "ok", 200

    def put(self, block_id):
        found_block = CondoBlock.query.get(block_id)
        if found_block == None:
            return "Block not found", 404

        parser = reqparse.RequestParser()
        parser.add_argument("block_id")
        parser.add_argument("block_name")
        args = parser.parse_args()

        found_block.block_name = args['block_name']
        database.db.session.commit()


class CondoResource(Resource):
    ConsoResource_fields ={
        'block_id' : fields.String,
        'block_name' : fields.String
    }

    @marshal_with(ConsoResource_fields, envelope='condoblocks')
    def get(self):
        return CondoBlock.query.all()



    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("block_id")
        parser.add_argument("block_name")

        args = parser.parse_args()

        condoBlock = CondoBlock(**args)

        found_block = CondoBlock.query.get(args['block_id'])

        if args['block_id'].strip() == "":
            return "Block ID is blank", 509

        if  args['block_name'].strip() == "":
            return "Block Name is blank", 509

        if not  found_block == None:
            return "Block ID alr exists", 409


        if CondoBlock.query.filter(CondoBlock.block_name==args['block_name']).count() != 0:
            return "Block Name alr exists", 409


        database.db.session.add(condoBlock)
        database.db.session.commit()

        return jsonify({"ok" : "ok"})
