from flask import jsonify, request
from flask_restful import Resource,  marshal_with, fields, reqparse

from models.visitor import *

from database import database

VisitorResource_fields = {
    'contact_number': fields.String,
    'visitor_name': fields.String,
    'ic_no': fields.String
}


VisitorUnitResource_fields = {
    'id' : fields.Integer,
    'contact_number': fields.String,
    'ic_no': fields.String,
    'block_id': fields.String,
    'unit_name': fields.String,
    'date_visited' : fields.DateTime,
    'date_checkout' : fields.DateTime
}


class ReportManagementResource(Resource):
    @marshal_with( {'visitorUnit':

                            fields.Nested({'id':fields.Integer,
                                           'block_id' : fields.String,
                                           'unit_name' : fields.String,
                                           'contact_number' : fields.String,
                                           'date_visited' : fields.DateTime,
                                           'date_checkout' : fields.DateTime})
                        ,
                    'visitor' : fields.Nested({'visitor_name' : fields.String})}, envelope='reports')
    def get(self):

        listOfFields = [("block_id", CondoUnit.block_id),
                        ("block_name", CondoBlock.block_name),
                        ("unit_name", CondoUnit.unit_name),
                        ("visitor_name", Visitor.visitor_name),
                        ("contact_number", Visitor.contact_number),
                        ("ic_no", Visitor.ic_no)
                        ]

        checkin_date_end = request.args.get("checkin_date_end")
        checkin_date_start = request.args.get("checkin_date_start")
        checkout_date_start = request.args.get("checkout_date_start")
        checkout_date_end = request.args.get("checkout_date_end")

        see_not_checkedout = request.args.get("see_not_checkedout") == "true"
        print(request.args.get("see_not_checkedout") )



        page = request.args.get("page")


        if page == None:
            page = 1

        myQuery = database.db.session.query(VisitorUnit, Visitor).\
            join(Visitor).join(CondoUnit).join(CondoBlock)

        for field_name, field in listOfFields:
            value = request.args.get(field_name)
            if value != None and value.strip() != "":
                print(value)
                myQuery = myQuery.filter(field.like('%' + value + '%'))


        if checkin_date_start != None and  checkin_date_start.strip() != "":
            checkin_date_start = datetime.datetime.strptime(checkin_date_start, '%Y-%m-%dT%H:%M:%SZ');
            myQuery = myQuery.filter(VisitorUnit.date_visited >= checkin_date_start)

        if checkin_date_end != None and  checkin_date_end.strip() != "":
            checkin_date_end = datetime.datetime.strptime(checkin_date_end, '%Y-%m-%dT%H:%M:%SZ');
            myQuery = myQuery.filter(VisitorUnit.date_visited <= checkin_date_end)

        if checkout_date_start != None and  checkout_date_start.strip() !=  "":
            checkout_date_start = datetime.datetime.strptime(checkout_date_start, '%Y-%m-%dT%H:%M:%SZ');
            myQuery = myQuery.filter(VisitorUnit.date_checkout >= checkout_date_start)

        if checkout_date_end != None and  checkout_date_end.strip() != "":
            checkout_date_end = datetime.datetime.strptime(checkout_date_end, '%Y-%m-%dT%H:%M:%SZ');
            myQuery = myQuery.filter(VisitorUnit.date_checkout <= checkout_date_end)

        if see_not_checkedout:
            print(see_not_checkedout)
            myQuery = myQuery.filter(VisitorUnit.date_checkout == None)

        listOfRows = []
        for visitorUnit, visitor in myQuery.all():
            listOfRows.append({'visitorUnit' : visitorUnit, 'visitor' : visitor});

        return listOfRows





class VisitorUnitCountResource(Resource):
    def get(self):
        block_id = request.args.get("block_id")
        unit_name = request.args.get("unit_name")

        return VisitorUnit.query.filter(VisitorUnit.block_id == block_id, VisitorUnit.unit_name == unit_name,
                                        VisitorUnit.date_checkout == None).count()



class VisitorVisitResource(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("unit_name")
        parser.add_argument("ic_no")
        parser.add_argument("contact_number")
        parser.add_argument("block_id")
        args = parser.parse_args()

        vu = VisitorUnit(**args)

        database.db.session.add(vu);
        database.db.session.commit()

class VisitorUnitCheckoutResource(Resource):
    def patch(self, visitor_unit_id):

        parser = reqparse.RequestParser()
        parser.add_argument("date_checkout")

        args = parser.parse_args()
        date_checkout = args['date_checkout']

        print(date_checkout)
        date_checkout = datetime.datetime.strptime(date_checkout, '%Y-%m-%dT%H:%M:%SZ');

        found_visitorunit = VisitorUnit.query.get(visitor_unit_id);

        if found_visitorunit == None:
            return "Visitor unit ID not found", 404

        if found_visitorunit.date_visited > date_checkout:
            return "Date checkout is earlier than date visited", 509

        found_visitorunit.date_checkout =date_checkout
        database.db.session.add(found_visitorunit);
        database.db.session.commit()

class VisitorVisitResource(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("unit_name")
        parser.add_argument("ic_no")
        parser.add_argument("contact_number")
        parser.add_argument("block_id")
        args = parser.parse_args()

        vu = VisitorUnit(**args)
        vu.date_visited = datetime.datetime.utcnow()

        lastVuUnit = database.db.session.query(VisitorUnit).join(Visitor).filter(VisitorUnit.ic_no == args['ic_no'],
                                                                    VisitorUnit.contact_number == args['contact_number'],
                                                                    VisitorUnit.date_checkout == None).first()

        print(lastVuUnit)
        if lastVuUnit != None:
            return "The last visit at " + lastVuUnit.block_id + " - "  + lastVuUnit.unit_name + " is not checked out yet", 409

        block_id = args['block_id']
        unit_name = args['unit_name']
        visitorCount = VisitorUnit.query.filter(VisitorUnit.block_id == block_id, VisitorUnit.unit_name == unit_name,
                                 VisitorUnit.date_checkout == None).count()

        if visitorCount >= 8:
            return "This unit has 8 visitors now, please wait for other check out.", 409

        database.db.session.add(vu);
        database.db.session.commit()
        return "ok"

    @marshal_with(VisitorUnitResource_fields, envelope='visitorunits')
    def get(self):
        ic_no = request.args.get("ic_no")
        contact_number = request.args.get("contact_number")
        no_record = request.args.get("no_record")

        if no_record ==None:
            no_record = 3


        return database.db.session.query(VisitorUnit).join(Visitor).filter(VisitorUnit.ic_no == ic_no,
                                       VisitorUnit.contact_number == contact_number).\
                                            order_by(VisitorUnit.date_visited.desc()).limit(no_record).all()

class VisitorResourceIndividual(Resource):

    @marshal_with(VisitorResource_fields, envelope='visitor')
    def get(self, contact_number, ic_no):
        found_visitor =  Visitor.query.get((contact_number, ic_no))
        if found_visitor == None:
            return "Visitor not found", 404

        return found_visitor

    # def delete(self, block_id, unit_name):
    #     found_unit = CondoUnit.query.get((block_id, unit_name))
    #     if found_unit == None:
    #         return "Unit not found", 404
    #
    #     database.db.session.delete(found_unit)
    #     database.db.session.commit();
    #
    #     return "ok", 200

    def put(self,  contact_number, ic_no):
        found_visitor = Visitor.query.get((contact_number, ic_no))

        if found_visitor == None:
            return "Visitor not found", 404

        parser = reqparse.RequestParser()
        parser.add_argument("contact_number")
        parser.add_argument("ic_no")
        parser.add_argument("visitor_name")

        args = parser.parse_args()

        found_visitor.contact_number = args['contact_number']
        found_visitor.visitor_name = args['visitor_name']
        found_visitor.ic_no = args['ic_no']

        database.db.session.commit()


class VisitorResource(Resource):


    @marshal_with(VisitorResource_fields, envelope='visitors')
    def get(self):
        return CondoUnit.query.all()



    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("contact_number")
        parser.add_argument("ic_no")
        parser.add_argument("visitor_name")

        args = parser.parse_args()


        visitor = Visitor(**args)

        found_visitor = Visitor.query.get((args['contact_number'],args['ic_no']) )

        if not  found_visitor == None:
            return "Visitor with this name already exists", 409

        database.db.session.add(visitor)
        database.db.session.commit()

        return jsonify({"ok" : "ok"})
