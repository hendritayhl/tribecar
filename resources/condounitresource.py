from flask import jsonify, request
from flask_restful import Resource,  marshal_with, fields, reqparse

from models.condoblock import CondoUnit

from database import database

CondoUnitResource_fields = {
    'block_id': fields.String,
    'unit_name': fields.String,
    'contact_person': fields.String,
    'contact': fields.String
}



class CondoUnitResourceIndividual(Resource):

    @marshal_with(CondoUnitResource_fields, envelope='condounit')
    def get(self, block_id, unit_name):
        found_unit=  CondoUnit.query.get((block_id, unit_name))
        if found_unit == None:
            return "Unit not found", 404

        return found_unit

    def delete(self, block_id, unit_name):
        found_unit = CondoUnit.query.get((block_id, unit_name))
        if found_unit == None:
            return "Unit not found", 404

        database.db.session.delete(found_unit)
        database.db.session.commit();

        return "ok", 200

    def put(self, block_id, unit_name):
        found_unit = CondoUnit.query.get((block_id, unit_name))

        if found_unit == None:
            return "Unit not found", 404

        parser = reqparse.RequestParser()
        parser.add_argument("block_id")
        parser.add_argument("unit_name")
        parser.add_argument("contact_person")
        parser.add_argument("contact")

        args = parser.parse_args()

        found_unit.unit_name = args['unit_name']
        found_unit.contact_person = args['contact_person']
        found_unit.contact = args['contact']

        database.db.session.commit()




class CondoUnitResource(Resource):


    @marshal_with(CondoUnitResource_fields, envelope='condounits')
    def get(self):
        block_id = request.args.get('block_id')

        if block_id:
            return CondoUnit.query.filter(CondoUnit.block_id == block_id).all()
        return CondoUnit.query.all()



    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("block_id")
        parser.add_argument("unit_name")
        parser.add_argument("contact_person")
        parser.add_argument("contact")

        args = parser.parse_args()
        condoUnit = CondoUnit(**args)



        found_unit = CondoUnit.query.get((args['block_id'],args['unit_name']) )

        if args['unit_name'].strip() == "":
            return "Unit Name is blank", 409

        if not  found_unit == None:
            return "Block ID with this unit alr exists", 409

        database.db.session.add(condoUnit)
        database.db.session.commit()

        return jsonify({"ok" : "ok"})
