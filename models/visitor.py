import dataclasses
import datetime

from sqlalchemy import Column, Integer, String, ForeignKey, ForeignKeyConstraint, DateTime
from sqlalchemy.orm import relationship

from database.database import db

from dataclasses import dataclass
from  models.condoblock import *

@dataclass
class Visitor( db.Model):
    __tablename__ = 'visitor'
    contact_number = Column(String(50), primary_key=True)
    ic_no = Column(String(50), primary_key=True)
    visitor_name = Column(String(100))
    visited_units = relationship("CondoUnit", secondary="vistor_unit", backref="visitors", lazy="select")


@dataclass
class VisitorUnit(db.Model):
    __tablename__ ="vistor_unit"
    id = Column(Integer, primary_key=True, autoincrement=True)
    block_id = Column(String(10), ForeignKey(CondoBlock.block_id), nullable=False)
    unit_name = Column(String(20), nullable=False)
    contact_number = Column(String(50))
    ic_no = Column(String(50) )
    date_visited = Column(DateTime, default=datetime.datetime.utcnow())
    date_checkout = Column(DateTime, nullable=True)


    fk_visitor_unit_condounti = ForeignKeyConstraint(
        (block_id, unit_name),
        (CondoUnit.block_id, CondoUnit.unit_name)  ),

    fk_visitor_unit_visitor = ForeignKeyConstraint(
        (contact_number, ic_no),
        (Visitor.contact_number, Visitor.ic_no)  ),



