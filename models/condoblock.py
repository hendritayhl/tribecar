from sqlalchemy import Column, Integer, String, ForeignKey
from database.database import db

from dataclasses import dataclass

@dataclass
class CondoBlock( db.Model):
    __tablename__ = 'condoblock'

    block_id : str
    block_name : str

    block_id = Column(String(10), primary_key=True)
    block_name = Column(String(50))

    def __init__(self, block_id=None, block_name=None):
        self.block_id = block_id
        self.block_name = block_name

@dataclass
class CondoUnit(db.Model):
    __tablename__ = 'condounit'

    block_id = Column(String(10), ForeignKey(CondoBlock.block_id), primary_key=True, nullable=False)
    unit_name = Column(String(20), primary_key=True, nullable=False)
    contact_person = Column(String(100))
    contact = Column(String(100))

